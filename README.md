# Ansible role: tool.boostrap_playbook

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![CI Status](https://img.shields.io/badge/CI-success-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Cbz-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: Boilerplate](https://img.shields.io/badge/Tech-Boilerplate-orange)
![Tag: Bootstrap](https://img.shields.io/badge/Tech-Bootstrap-orange)

An Ansible role to bootstrap and create other playbooks.

The Playbook Bootstrap role automates the creation of a basic structure for an Ansible playbook. It sets up the necessary folders, files, and configurations to kickstart your playbook development process. Key features of this role include:

Predefined Folder Structure: The role creates a well-organized folder structure, including molecule, tests, and other necessary directories. This ensures consistency and facilitates modular organization of your playbook components.

Code Quality Tools: Configuration files such as .ansible-lint, .ansible.cfg, and .yamllint are included to enforce code quality standards and promote best practices in your playbook development.

Collaboration Support: The inclusion of a CODEOWNERS file enables seamless collaboration and ownership assignment within your Git repository.

Molecule Integration: The role comes preconfigured with Molecule, a testing framework for Ansible. It includes default Molecule scenarios and GitLab CI configuration for streamlined testing and continuous integration.

By utilizing the Playbook Bootstrap role, you can expedite the creation of new Ansible playbooks, maintain code quality, and foster efficient collaboration within your development team.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-05-08: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez

### 2023-05-30: Cryptographic update

* SSL/TLS Materials are not handled by the role
* Certs/CA have to be installed previously/after this role use

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2024-02-20: Remastered

* Imported new CICD
* Rework global on readme
* Rename of vars __

### 2024-03-01: Rework

* You can now define your scenario
* Groups vars handled
* You can now define your requirements
* You can now define your test sequence
* You can now define your configuration list file
* You can now define docker images for your scenario
* You can now define how much hosts to create for each scenarios

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-10-07: Ansible Vault and automations

* Added one local Ansible Vault
* Edited gitignore file
* Add some commands in documentation

### 2024-11-24: Add become

* Add a become method and become parameter
* You can now use a different user that root for run and became root or whatever

### 2025-01-01: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
